# Markdownify Automation Script

The Markdownify Automation Script is a Python script designed to automate the process of generating Markdown files from history commands. It interacts with a remote server to convert history commands into Markdown format. This README provides instructions on how to use the script and set it up.

## Table of Contents

- [Features](#features)
- [Prerequisites](#prerequisites)
- [Getting Started](#getting-started)
- [Usage](#usage)
- [Options](#options)
- [Dependencies](#dependencies)

## Features

- Automatically generate Markdown files from history commands.
- Save and manage history files remotely using an API.
- Option to customize the output directory and file name.
- Input API key for secure access to remote services.

## Prerequisites

Before using the Markdownify Automation Script, ensure you have the following prerequisites installed:

- Python 3.x
- `pip` package manager
- `requests` library (can be installed using `pip`)
- Internet connectivity to access the remote Markdownify service

## Getting Started

1. Clone or download this repository to your local machine:

   ```bash
   git clone https://gitlab.com/apajak/markdownify_app.git
   cd markdownify-app
   ```

2. Install manually the required dependencies:

   ```bash
   pip install -r requirements.txt
   ```

3. and/or Initialize the script by running it:

*(The script downloads the required dependencies automatically.)*

   ```bash
   python markdownify.py
   ```

   This will prompt you to enter your API key. The API key can be obtained from the [Markdownify service.](https://markdownify.arpanode.fr/register)


## Usage

The Markdownify Automation Script can be used in the following ways:

- **Interactive Mode**: Run the script without any command-line arguments to interactively enter the input file path and other options.

   ```bash
   python markdownify.py
   ```

- **Command-Line Mode**: Use command-line arguments to specify options without user interaction. For example, to automatically generate a Markdown file from a history file:

   ```bash
   python markdownify.py -a -f history.txt -o output_directory
   ```

   This command will automatically generate a Markdown file from the `history.txt` file and save it in the `output_directory`.

## Options

The script supports the following command-line options:

- `-f <input_file>`: Specify the input file containing history commands.
- `-a`: Automatically generate the input file with history commands and run the Markdown generation process.
- `-g`: Generate the input file with history commands but do not run the Markdown generation process.
- `-o <output_directory>`: Specify the output directory for the generated Markdown files.
- `-r <rename>`: Rename the output Markdown file.
- `-h, --help`: Display help and usage information.

## Dependencies

The Markdownify Automation Script relies on the following Python libraries:

- `requests`: Used for making HTTP requests to interact with the Markdownify service.

These dependencies are listed in the `requirements.txt` file and can be installed using `pip`.

DME to provide more specific instructions or additional information about your script.