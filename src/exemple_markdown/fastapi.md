

# Installation of Automd

## Prerequisites

* Python 3.10
* Python virtual environment

## Installation

1. Create a virtual environment:
    ```
    cd .virtualenvs/
    python -m venv automd
    source automd/bin/activate
    ```

2. Install the required packages:
    ```
    cd automd/
    pip install -r requirements.txt
    ```

3. Start the application:
    ```
    cd ap/api/
    uvicorn app:app reload port=8004
    ```

4. Create a systemd service:
    ```
    cd
    sudo nano /etc/systemd/system/automd.service
    sudo systemctl daemon-reload
    sudo systemctl start automd
    sudo systemctl status automd
    sudo systemctl enable automd
    ```

5. Install additional packages:
    ```
    pip3 install --upgrade pip setuptools wheel
    pip3 install --upgrade httpie glances
    pip3 install --upgrade gunicorn uvloop httptools
    ```

6. Run the bash script:
    ```
    cd automd/app/src/
    bash autoMD.sh -a
    ```

7. Create a configuration file for nginx:
    ```
    cd /etc/nginx/conf.d/
    sudo nano automdApi.conf
    sudo systemctl restart nginx.service
    ```

8. Install SSL certificate:
    ```
    sudo certbot --nginx
    ```

9. Update the nginx configuration file:
    ```
    cd /etc/nginx/conf.d/
    sudo nano automdApi.conf
    sudo systemctl restart nginx.service
    ```