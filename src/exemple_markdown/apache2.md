

# Installing Apache2

Apache2 is a popular open-source web server used to host websites. This guide will walk you through the steps of installing Apache2 on a Linux system.

## Prerequisites

Before you begin, make sure you have the following:

- A Linux system with root access
- An internet connection

## Step 1: Update the System

The first step is to update the system. This will ensure that all the latest packages are installed. To do this, run the following command:

```
sudo apt update
```

## Step 2: Install Apache2

Once the system is updated, you can install Apache2. To do this, run the following command:

```
sudo apt install apache2
```

## Step 3: Allow Apache2 Through the Firewall

Next, you need to allow Apache2 through the firewall. To do this, run the following commands:

```
sudo ufw app list
sudo ufw allow 'Apache'
sudo ufw status
```

## Step 4: Start Apache2

Once the firewall is configured, you can start Apache2. To do this, run the following commands:

```
sudo systemctl status apache2
sudo systemctl start apache2
sudo systemctl enable apache2
```

## Step 5: Create a Directory for Your Website

Next, you need to create a directory for your website. To do this, run the following command:

```
sudo mkdir /var/www/your_domain
```

## Step 6: Set the Permissions for the Directory

Once the directory is created, you need to set the permissions for the directory. To do this, run the following commands:

```
sudo chown -R $USER:$USER /var/www/your_domain
sudo chmod -R 755 /var/www/your_domain
```

## Step 7: Create an Index File

Next, you need to create an index file. To do this, run the following command:

```
sudo nano /var/www/your_domain/index.html
```

## Step 8: Check the Index File

Once the index file is created, you can check it. To do this, run the following command:

```
sudo cat /var/www/your_domain/index.html
```

## Step 9: Create a Virtual Host File

Next, you need to create a virtual host file. To do this, run the following command:

```
sudo nano /etc/apache2/sites-available/your_domain.conf
```

## Step 10: Test the Configuration

Once the virtual host file is created, you can test the configuration. To do this, run the following command:

```
sudo apache2ctl configtest
```

## Step 11: Restart Apache2

Finally, you need to restart Apache2. To do this, run the following command:

```
sudo systemctl restart apache2
```

## Conclusion

Congratulations! You have successfully installed Apache2 on your Linux system.