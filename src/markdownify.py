#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Created By  : LexIt
# Created Date: 2023/09/17
# version ='2.0'
# ---------------------------------------------------------------------------
""" Markdownify is a tool that allows you to generate a Markdown file from a text file containing history commands."""
# ---------------------------------------------------------------------------
import argparse
import requests
import sys
import threading
import os
import subprocess
import time
import pkg_resources
import getpass

# Global variables
host = "https://markdownify.api.arpanode.fr"
script_dir = os.path.dirname(os.path.abspath(__file__))
requirement_txt_path = os.path.join(script_dir, "requirements.txt")
file = ""
auto = False
gen = False
output_dir = ""
rename = ""


# Parse command line arguments
parser = argparse.ArgumentParser()
parser.add_argument("-f", help="Specify the input file for markdownify.py")
parser.add_argument("-a", action="store_true", help="Automatically generate the input file with history "
                                                    "commands and run markdownify.py", default=False)
parser.add_argument("-g", action="store_true", help="Generate the input file with history commands but do"
                                                    " not run markdownify.py", default=False)
parser.add_argument("-o", help="Specify the output directory for the generated Markdown files")
parser.add_argument("-r", help="Rename the output Markdown file")
args = parser.parse_args()

def init_key():
    """Get API key from local file or ask for it"""
    # search for local API key
    if os.path.exists("api_key.txt"):
        # read the API key from the file
        with open("api_key.txt", "r") as f:
            api_key = f.read()
        return api_key
    else:
        print("API key not found locally")
        # ask for the API key from the user
        api_key = getpass.getpass("Enter your API key: ")
        # ask for store locally or not
        store = input("Do you want to store your API key locally? (y/n): ")
        if store == "y":
            # store the API key locally
            with open("api_key.txt", "w") as f:
                f.write(api_key)
            print("API key stored locally")
        else:
            print("API key not stored locally")
        return api_key

def check_key(api_key):
    """Check from markdownify API if the API_key is valid"""
    try:
        # Send api key to /whoami for getting username
        url = host + "/whoami?api_key=" + api_key
        r = requests.post(url)
        if r.status_code == 200:
            username = r.json()["username"]
            return username
        else:
            print("API key is invalid")
            sys.exit()
    except requests.exceptions.RequestException as e:
        print("Request error:", e)
        sys.exit()

def main():
    """Main function"""
    global file, auto, gen, output_dir, rename, host
    api_key = init_key()
    username = check_key(api_key)
    print("Welcome", username)

    # Set the input file path
    if args.f:
        file = args.f

    # Enable automatic mode
    if args.a:
        # Generate input file with history commands
        subprocess.run(["history", "-w", "commande.txt"])
        # Set the input file path
        file = "./history.txt"
        auto = True

    # Enable generation mode
    if args.g:
        # Generate input file with history commands and exit
        subprocess.run(["history", "-w", "commande.txt"])
        gen = True

    # Check if generation mode is enabled and output directory path is set generate input file with history commands at
    # the output directory
    if args.g and args.o:
        # Generate input file with history commands
        subprocess.run(["history", "-w", os.path.join(args.o, "history.txt")])

        # Set the input file path
        file = os.path.join(args.o, "history.txt")
        gen = True

    # Check if the input file path is set
    if not file and not (auto or gen):
        # Display error message and exit
        print("Error: -f option is required when automatic or generation mode is not enabled", file=sys.stderr)
        sys.exit(1)

    # Check if the input file exists
    if file and not os.path.isfile(file):
        # Display error message and exit
        print(f"Error: input file {file} does not exist", file=sys.stderr)
        sys.exit(1)

    # Check if the output directory path is set
    if args.o and not os.path.isdir(args.o):
        # Create the output directory if it does not exist
        os.makedirs(args.o)
    else:
        # Set the output directory path to the current directory
        output_dir = "./My_Markdowns"

    file_path = args.f
    file = {"file": open(file_path, "rb")}

    # save the history file
    save_url = f"{host}/save_history"
    response = requests.post(save_url, files=file, data={"api_key": api_key})
    if response.status_code == 200:
        history_id = response.json()["history_id"]
        print(f"History file saved with id {history_id}")
    else:
        print("Error: history file not saved", file=sys.stderr)
        sys.exit(1)


    generate_url = f"{host}/generate_markdown"
    file_name = os.path.splitext(os.path.basename(file_path))[0]

    output_dir = args.o if args.o else "./My_Markdowns"
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    if args.r:
        file_name = args.r

    # Start the spinner
    def spinner():
        spin_chars = "/-\\|"
        i = 0
        while not spinner_stop.is_set():
            sys.stdout.write("\rGenerating Markdown " + spin_chars[i])
            sys.stdout.flush()
            i = (i + 1) % len(spin_chars)
            time.sleep(0.1)

    response = None

    # Check if the input file is a text file
    if not file_path.endswith(".txt"):
        print("Error: input file must be a text file", file=sys.stderr)

    # press enter to continue
    input("Press enter for generate your markdown file")
    # Create a thread to display the spinner
    spinner_stop = threading.Event()
    spinner_thread = threading.Thread(target=spinner)
    spinner_thread.start()

    try:
        # Send the request
        file = {"file": open(file_path, "rb")}
        response = requests.post(generate_url, files=file, data={"api_key": api_key})

        # Stop the spinner
        spinner_stop.set()
        spinner_thread.join()

        # Handle the response as needed
        if response.status_code == 200:
            print("\nMarkdown generation completed successfully.")
        else:
            print("\nError: Markdown generation failed (status code {}).".format(response.status_code))
    except Exception as e:
        print("\nError: {}".format(e))

    if response.status_code == 200:
        # Save the response to a file in the output directory
        output_path = os.path.join(output_dir, f"{file_name}.md")
        with open(output_path, "w") as f:
            f.write(response.text)
        print(f"Markdown file generated at {output_path}")

        output_file = {"file": open(output_path, "rb")}
        # Send the file to the server
        # save the history file
        markdown_save_url = f"{host}/save_markdown"
        response = requests.post(markdown_save_url, files=output_file, data={"api_key": api_key,"history_id":history_id})
        if response.status_code == 200:
            print("Markdown file saved")
        else:
            print("Error: history file not saved", file=sys.stderr)
            sys.exit(1)


    else:
        print(f"Error: {response.status_code} {response.reason}", file=sys.stderr)
        sys.exit(1)

    if auto:
        # Remove the input file
        os.remove(file)

def check_dependencies():
    """Check if the required dependencies are installed"""
    with open('requirements.txt') as f:
        required_packages = f.read().splitlines()

    installed_packages = [pkg.key for pkg in pkg_resources.working_set]

    missing_packages = [pkg for pkg in required_packages if pkg not in installed_packages]

    if missing_packages:
        print("The following dependencies are missing:")
        for pkg in missing_packages:
            print(pkg)
        return False
    else:
        return True
def install_dependencies():
    """Install the required dependencies from requirements.txt using pip"""
    print("Installing dependencies...")
    try:
        with open(requirement_txt_path) as f:
            requirements = f.read().splitlines()
        for package in requirements:
            print(f"Installing {package}")
            subprocess.check_call(['pip', 'install', package])
        print("Dependencies installed successfully.")
    except Exception as e:
        print(f"Error installing dependencies: {e}")
        sys.exit(1)

if __name__ == "__main__":
    if not check_dependencies():
        install_dependencies()
    main()
