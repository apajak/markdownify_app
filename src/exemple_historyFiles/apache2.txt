sudo apt update
sudo apt install apache2
sudo ufw app list
sudo ufw allow 'Apache'
sudo ufw status
sudo systemctl status apache2
sudo systemctl start apache2
sudo systemctl enable apache2
sudo mkdir /var/www/your_domain
sudo chown -R $USER:$USER /var/www/your_domain
sudo chmod -R 755 /var/www/your_domain
sudo nano /var/www/your_domain/index.html
sudo cat /var/www/your_domain/index.html
sudo nano /etc/apache2/sites-available/your_domain.conf
sudo apache2ctl configtest
sudo systemctl restart apache2